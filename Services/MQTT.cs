﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.Json;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using WebApplication.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication.Controllers
{
    public class MQTT
    {
        private readonly IServiceScopeFactory _scopeFactory;
        private MqttClient _client;

        public MQTT(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
            Setup();
        }

        public void Setup()
        {
            //https://www.hivemq.com/blog/mqtt-client-library-encyclopedia-m2mqtt/
            Console.WriteLine("[MQTT] Its working");

            //Подключение к брокеру
            _client = new MqttClient("broker.mqtt-dashboard.com");
            string clientId = Guid.NewGuid().ToString();
            _client.Connect(clientId);
            // -----

            SubscribeTopic("/altana/test_output");
            PublishMessage("/altana/test_output", "Hello pidor");

            _client.MqttMsgPublishReceived += ReceiveMessage;
        }

        private void ReceiveMessage(object sender, MqttMsgPublishEventArgs e)
        {

            Console.WriteLine(e.Topic);

            string topic = e.Topic;

            try
            {
                var jsonString = JsonSerializer.Deserialize<Device>(e.Message);
                Console.WriteLine("[MQTT] Client value: " + jsonString.clientValue);

                var message = System.Text.Encoding.Default.GetString(e.Message);
                Console.WriteLine("[MQTT] Message received: " + message);

                // TODO: Добавить запись в лог и раздельную десереализацию
                AddToDatabase(jsonString, topic);
            }
            catch
            {
                var message = System.Text.Encoding.Default.GetString(e.Message);
                Console.WriteLine("[MQTT] This is not a JSON! " + message);
            }
        }

        private void AddToDatabase(Device message, String topic)
        {
            // FIXME: Костыль
            using (var scope = _scopeFactory.CreateScope())
            {
                var db = scope.ServiceProvider.GetRequiredService<ApplicationContext>();

                //Device search_device = new Device();
                //search_device = db.Devices.First(u => u.topicValue == topic);

                //switch (search_device.topicValue)
                //{
                //    case "sensor":
                //        Console.WriteLine("[LOG]");
                //        Log new_log = new Log(message.);
                //        db.Logs.Add(message);
                //        break;
                //    case "other":
                //        Console.WriteLine("[OTHER]");
                //        break;
                //    default:
                //        Console.WriteLine("Default case");
                //        break;
                //}

                //Console.WriteLine(search_device.clientValue);


                db.Devices.Add(message);
                db.SaveChanges();

                // when we exit the using block,
                // the IServiceScope will dispose itself 
                // and dispose all of the services that it resolved.
            }
        }

        public void PublishMessage(string topic, string message)
        {
            // Отправка сообщения в топик
            // TODO: Выделить в функцию по аналогии с Подпиской или переписать по документации по ссылке
            ushort msgId = _client.Publish(
                    topic,
                    System.Text.Encoding.UTF8.GetBytes(message),
                    MqttMsgBase.QOS_LEVEL_AT_LEAST_ONCE,
                    true
                );
            // -----
        }
        public void SubscribeTopic(string topic)
        {
            // Подписка на топик
            Console.WriteLine("[MQTT] Subscriber: " + topic);
            _client.Subscribe(
                    new string[] { topic },
                    new byte[] { MqttMsgBase.QOS_LEVEL_AT_LEAST_ONCE }
                );
            // -----
        }
    }
}

