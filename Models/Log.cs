﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public class Log
    {
        public int Id { get; set; }
        public string clientValue { get; set; }
        public string temperatureValue { get; set; }
        public string humidityValue { get; set; }

    }
}