﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public class Device
    {
        public int Id { get; set; }
        public string clientValue { get; set; }
        public string topicValue { get; set; }
        public string typeValue { get; set; }

    }
}