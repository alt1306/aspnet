﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class TestController : Controller
    {
        private readonly ILogger<TestController> _logger;
        private MQTT _mqtt;

        public TestController(ILogger<TestController> logger, MQTT mqtt)
        {
            _logger = logger;
            _mqtt = mqtt;
        }

        public IActionResult Index()
        {
            _mqtt.PublishMessage("/altana/test_mqtt2/output", "hi ksta");
            return View();
        }

        [HttpGet]
        public IActionResult Test()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Test(string login, string password)
        {
            string authData = $"Login: {login}   Password: {password}";
            Console.WriteLine(authData);

            TestFunction();
            return Content(authData);
        }

        private string TestFunction()
        {
            return "Hello world";
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
