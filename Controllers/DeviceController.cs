﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    // Для выборки без повторений
    class ItemEqualityComparer : IEqualityComparer<Device>
    {
        public bool Equals(Device x, Device y)
        {
            // Two logs are equal if their keys are equal.
            return x.clientValue == y.clientValue;
        }

        public int GetHashCode(Device obj)
        {
            return obj.clientValue.GetHashCode();
        }
    }

    public class DeviceController : Controller
    {
        #region Static
        private readonly ILogger<DeviceController> _logger;
        private ApplicationContext _db;
        public DeviceController(ILogger<DeviceController> logger, ApplicationContext context)
        {
            _logger = logger;
            _db = context;
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        #endregion
        //Работа с БД
        [ActionName("Index")]
        public async Task<IActionResult> Index()
        {
            // https://www.dotnetperls.com/duplicates
            List<Device> list = new List<Device>();
            list = _db.Devices.ToList();

            var logs = list.Distinct(new ItemEqualityComparer());

            //foreach (var item in logs)
            //{
            //    Console.WriteLine($"DISTINCT ITEM = {item.clientValue} {item.Id}");
            //}

            return View(logs);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id != null)
            {
                //State state = await _db.States.FirstOrDefaultAsync(p => p.Id == id);
                Device device = await _db.Devices.Where(u => u.Id == id).FirstOrDefaultAsync();
                if (device != null)
                    return View(device);
            }
            return NotFound();
        }

    }
}